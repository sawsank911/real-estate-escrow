const { expect } = require("chai");
const { ethers } = require("hardhat")

const tokens = (n) => {
    return ethers.utils.parseUnits(n.toString(), "ether")
}

const ether = tokens

describe('RealEstate', () => {
    let realEstate, escrow;
    let accounts, deployer, seller, buyer, inspector, lender;
    let nftID = 1;
    let transaction;
    let purchasePrice = ether(100);
    let escrowAmount =  ether(100*0.20);

    beforeEach(async () => {

        //Setup accounts
        accounts = await ethers.getSigners()
        deployer = accounts[0]
        seller = deployer
        buyer = accounts[1]
        lender = accounts[2]
        inspector = accounts[3]

        // Load contracts
        const RealEstate = await ethers.getContractFactory("RealEstate")
        const Escrow = await ethers.getContractFactory("Escrow")

        // deploy contracts
       realEstate = await RealEstate.deploy()
       escrow =  await Escrow.deploy(
            realEstate.address,
            nftID, 
            purchasePrice,
            escrowAmount,
            seller.address, 
            buyer.address,
            lender.address,
            inspector.address
        )

       // Approve nft transfer for nft
       transaction = await realEstate.connect(seller).approve(escrow.address, nftID)
       await transaction.wait()
    })

    describe('Deploy', () => {
        it("Sends an NFT to seller/deployer", async () => {
            expect(await realEstate.ownerOf(nftID)).to.equal(seller.address)
        })
    })

    describe('Selling real estate', () => {
        let balance;
        it("executes a successful transaction", async () => {
            //Log seller balance
            console.log("Seller balance before execution", ethers.utils.formatEther(await ethers.provider.getBalance(seller.address)))
            //Seller should be owner of nft before selling
            expect(await realEstate.ownerOf(nftID)).to.equal(seller.address)
            // Buyer deposits the escrow amount
            transaction = await escrow.connect(buyer).depositEarnest({value: escrowAmount})
            await transaction.wait()
            //check balance of contract
            balance = await escrow.getBalance()
            console.log("Escrow balance: ", ethers.utils.formatEther(balance))
            //send inspector report (passed|not passed)
            transaction = await escrow.connect(inspector).updateInspectionStatus(true)
            await transaction.wait()
            //Approve sale from buyer
            transaction = await escrow.connect(buyer).approveSale()
            await transaction.wait()
            //Approve sale from seller
            transaction = await escrow.connect(seller).approveSale()
            await transaction.wait()
            //Lender provides rest of the fund
            transaction = await lender.sendTransaction({to: escrow.address, value: ether(80)})
            await transaction.wait()
            //Approve sale from lender
            transaction = await escrow.connect(lender).approveSale()
            await transaction.wait()
            //Finalize the sale
            transaction = await escrow.connect(buyer).finalizeSale()
            await transaction.wait()
            //New owner should be the seller
            expect(await realEstate.ownerOf(nftID)).to.equal(buyer.address)
            //Check to see if seller has received the funds
            //Log seller balance
            console.log("Seller balance after execution", ethers.utils.formatEther(await ethers.provider.getBalance(seller.address)))
        })
    })
})