// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface IERC721 {
    function transferFrom(address _from, address _to, uint256 _id) external;
}

contract Escrow {

    address public nftAddress;
    uint256 public nftID;
    uint256 public purchasePrice;
    uint256 public escrowAmount;
    address payable public seller;
    address payable public buyer;
    address public lender;
    address public inspector;


    modifier onlyBuyer(){
        require(msg.sender == buyer, "Only buyer can call this function");
        _;
    }

    modifier onlyInspector(){
        require(msg.sender == inspector, "Only inspector can call this function");
        _;
    }
    bool public inspectionPassed = false;
    mapping(address => bool) public approval;

    receive() external payable {}

    constructor(
        address _nftAddress,
        uint256 _nftID,
        uint256 _purchasePrice,
        uint256 _escrowAmount,
        address payable _seller,
        address payable _buyer,
        address _lender,
        address _inspector
    ){
        nftAddress = _nftAddress;
        nftID = _nftID;
        purchasePrice = _purchasePrice;
        escrowAmount = _escrowAmount;
        seller = _seller;
        buyer = _buyer;
        lender = _lender;
        inspector = _inspector;
    }

    function depositEarnest() public payable onlyBuyer {
        require(msg.value >= escrowAmount, "Amount must satisfy needed amount");
    }

    function updateInspectionStatus(bool _passed) public onlyInspector {
        inspectionPassed = _passed;
    }

    function approveSale() public{
        approval[msg.sender] = true;
    }

    function getBalance() public view returns (uint){
        return address(this).balance;
    }

    function finalizeSale() public {
        require(inspectionPassed, "Inspection is not passed");
        require(approval[buyer], "Buyer approval needed");
        require(approval[lender], "Lender approval needed");
        require(approval[seller], "Seller approval needed");
        require(address(this).balance >= purchasePrice, "Not sufficient funds in contract");

        (bool success, ) = payable(seller).call{value: address(this).balance}("");
        require(success);
        //Transfer ownership of property
        IERC721(nftAddress).transferFrom(seller, buyer, nftID);
    }
}